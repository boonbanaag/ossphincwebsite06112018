<?php
require 'auth.php';
// HTML authentication  
authHTML();

?>
<html>
<head>

<script type="text/javascript">window.$crisp=[];window.CRISP_WEBSITE_ID="247a2797-6ac5-448d-8d0a-d23589a66e16";(function(){d=document;s=d.createElement("script");s.src="https://client.crisp.chat/l.js";s.async=1;d.getElementsByTagName("head")[0].appendChild(s);})();</script>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!--  <meta name="viewport" content="width=device-width, initial-scale=1">-->
  <meta name="keywords" content="ERPNext,QuickBooks Philippines,Accounting Software, ERP, OSSPHinc,Quezon City,
  xTuple,Solutions Provider,Systems Provider">
  <meta name="description" content="Software Solution Provider.">

    <title>OSSPHinc</title>
    <!--global css starts-->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/custom.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/panel.css"/>
    <link rel="stylesheet" type="text/css" href="css/mv_style.css"/>
    <link rel="stylesheet" type="text/css" href="css/carousel1.css">

    <!--end of global css-->
    <!--page level css starts-->
    <link rel="stylesheet" type="text/css" href="css/tabbular.css">
    <link rel="stylesheet" type="text/css" href="css/jquery.circliful.css">
    <link rel="stylesheet" type="text/css" href="vendors/owl-carousel/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="vendors/owl-carousel/owl.theme.css">

    

      <!-- Add fancyBox main CSS files -->
    <link rel="stylesheet" type="text/css" href="vendors/gallery/basic/source/jquery.fancybox.css?v=2.1.5" media="screen" />
    <!-- Add Button helper (this is optional) -->
    <link rel="stylesheet" type="text/css" href="vendors/gallery/basic/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" />
    <!-- Add Thumbnail helper (this is optional) -->
    <link rel="stylesheet" type="text/css" href="vendors/gallery/basic/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" />
    <!--end of page level css-->



  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/particles.js/2.0.0/particles.min.js"></script>
  <style type="text/css">
    body{background: white;}

  </style>
  <title>OpenSource Support</title>
</head>

    <header>
        <!-- Nav bar Start -->
        <div class="container-fluid header center-div">
      <div class="row">
              <nav id="mainNav" class="navbar navbar-default navbar-fixed-top" style="background-color: #FFFFFF;">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" style="margin-top: 20px;">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand page-scroll" href="index.html"><img src="images/logo.png" alt="OssPhinc"></a>
            </div>
<br>            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" style="padding-top: 10px;">
                <ul class="nav navbar-nav navbar-left">
      <!--      <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" style="font-size:14px; font-color:#ffffff;" href="#flexben">Services
            <span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
          <li><a class="page-scroll" href="#">Quickbooks Training</a></li>
              <li><a class="page-scroll" href="#">Managers Software Training</a></li> 

              <li><a class="page-scroll" href="#">Business Process Consultation</a></li> 
            </ul>
           </li>

          <li class="dropdown">
               <!-- <a class="dropdown-toggle" data-toggle="dropdown" style="font-size:14px; font-color:#ffffff;" href="#">Products 
            <span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
              <li><a class="page-scroll" href="#">Quickbooks</a></li>
              <li><a class="page-scroll" href="#">ERP</a></li> 
            </ul>
           </li>
          
          <li>
            <a class="page-scroll" style="font-size:14px;" href="#">Request for License</a>
          </li> -->
          
          <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" style="font-size:14px; font-color:#ffffff;" href="index.html#">About Us
            <span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
              <li><a class="page-scroll" href="album.html">Gallery</a></li>
              <li><a class="page-scroll" href="#">Our Team</a></li> 
            </ul>
           </li>

          <li>
            <a class="page-scroll" style="font-size:14px;" href="index.html#conts">Contact Us</a>
          </li>

        <!--   <li>
            <a class="page-scroll" style="font-size:14px;" href="#">Renewal of License</a>
          </li> -->

          <li>
            <a href="https://erp.ossphinc.com"> Login</a>
          </li>
           
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>
        </div>
    </div>
    </header>


<!-- Home Section <section id="home" class="parallax-section">
     <div id="particles-js"></div> -->
   

    
<!-- Particle Background -->
    



<br><br>

<section>
<div align="center">
<iframe src="https://calendar.zoho.com/embed/b57a8dac08560cdb67af1f4ec9d9457666e393052e856dac9643876fd98ba3e6595aecdfdb5cbfde4e53d59319ef7221?title=&type=6&l=en&tz=Asia/Manila&sh=[0,1,0]&c=0" width="750" height="300" frameborder="0" scrolling="no"></iframe>
</div>
</section>


<div class="copyright1" style="margin-top: 75px">
  <!--<div>
    <p>All Rights Reserved &copy; Opensource Support Phil. Inc. 2018</p>
  </div>-->
</div>

    <!-- //Footer Section End -->
    <!-- Copy right Section End -->
    <a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Return to top" data-toggle="tooltip" data-placement="left">
        <i class="livicon" data-name="plane-up" data-size="18" data-loop="true" data-c="#fff" data-hc="white"></i>
    </a>



</body>
</html>

<?php

?>